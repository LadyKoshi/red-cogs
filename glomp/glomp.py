import discord
import errno
import logging
import os
import random
from discord.ext import commands
from .utils.dataIO import dataIO
from .utils import checks

logger = logging.getLogger("red.glomp")
data_dir = os.path.join(os.getcwd(),'data','glomp')

class Glomp:
    _data_file = os.path.join(data_dir, 'data.json')
    _default_data = {
        '1': [
            '*runs at {user.mention} and hugs them intensely*',
        ],
    }

    def __init__(self, bot):
        self.bot = bot
        self.data = dataIO.load_json(self._data_file)

    @commands.group(pass_context=True, invoke_without_command=True)
    async def glomp(self, context, user:discord.Member, intensity:int=1):
        '''Glomp a person'''
        if context.invoked_subcommand is None:
            choice = '**Failed to glomp {user.mention}, no glomps for intensity={intensity}.** http://i0.kym-cdn.com/photos/images/newsfeed/000/906/455/51f.gif'
            choices = self.data.get(str(intensity))
            if choices:
                choice = random.choice(choices)
            response = choice.format(user=user, intensity=intensity)
            await self.bot.say(response)


    @glomp.command()
    @checks.serverowner_or_permissions(administrator=True)
    async def add(self, text:str, intensity:int):
        '''Add new glomp, requires mod permission'''
        intensity = str(intensity)
        if intensity not in self.data:
           self.data[intensity] = []
        self.data[intensity].append(text)
        dataIO.save_json(self._data_file, self.data)

def check_folders():
    dirs = [
        data_dir,
    ]
    for d in dirs:
        try:
            os.makedirs(d)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

def check_files():
    files = {
        Glomp._data_file: Glomp._default_data,
    }
    for filename,default_data in files.items():
        if not os.path.isfile(filename):
            logger.info("Creating empty {}".format(filename))
            dataIO.save_json(filename,default_data)
    

def setup(bot):
    check_folders()
    check_files()
    bot.add_cog(Glomp(bot))