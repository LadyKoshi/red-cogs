import discord
from discord.ext import commands
from __main__ import send_cmd_help, settings
import aiohttp
import logging
import json

red_logger = logging.getLogger("red")

class IdlePoring:
    def __init__(self, bot):
        self.bot = bot
        self.urls = {'search': 'http://idleporing.wikia.com/api/v1/Search/List'}
        self.logger = red_logger.getChild('idleporing')

    @commands.group(invoke_without_command=True, pass_context=True)
    async def idleporing(self, context, *, command):
        if context.invoked_subcommand is None:
            await send_cmd_help(context)

    @idleporing.group(invoke_without_command=True, pass_context=True)
    async def wiki(self, context, *, command):
        if context.invoked_subcommand is None:
            await send_cmd_help(context)

    @wiki.command(aliases=['find'], pass_context=True)
    async def search(self, context, *, search_terms : str):
        clean_search_terms = search_terms.strip()
        url = self.urls['search']
        params = {'query': clean_search_terms, 'limit': 5}
        
        try:
            result = await self.get_response(url, params)
            if result['total'] == 0:
                response = 'I didn\'t find anything matching "{}"'.format(search_terms)
            else:
                for item in result['items']:
                    if item['title'].lower().strip() == clean_search_terms.lower():
                        url = self.urls['page']
        except aiohttp.ClientError as e:
            self.logger.error(e)
            await self.bot.send_message(context.message.channel, "There was an error with the wikia API request")
        except Exception as e:
            self.logger.error(e)
        else:
            await self.bot.say(response) #TODO what is something?


    async def get_response(self, url, params=None):
        if params is None:
            params = {}

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params) as resp:
                if resp.status == 200:
                    data = json.loads(await resp.text())
                else:
                    raise Exception("Response: {}".format(resp.status), resp)#TODO Custom Errors?
        return data

def setup(bot):
    bot.add_cog(IdlePoring(bot))

