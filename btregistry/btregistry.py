import discord
from discord.ext import commands
import logging

logger = logging.getLogger("red.btregistry")

class BTRegistry:
    _data_path = 'data/btregistry/data.json'
    _settings_path = 'data/btregistry/settings.json'

    def __init__(self, bot):
        self.bot = bot
        self.settings = dataIO.load(_settings_path)
        
        if os.path.is_file(_data_path):
            pass


    @commands.group(name='bt', pass_context=True, aliases=['battleteam',])
    async def bt(self,ctx):
        server = ctx.message.server
        author = ctx.message.author

        if server not in self.data:
            self.data[server] = {}
        pass
    
    @bt.command(name='register', pass_context=True)
    async def bt_register(self, ctx, *, name: str, id: int, vacancies: int=4):
        self.save_data()
    
    @bt.command(name='list')
    async def bt_list(self,ctx):
        pass

    @bt.command(name='join')
    async def bt_join(self,ctx):
        pass

    @bt.command(name='add')
    async def bt_add(self,ctx):
        pass
        
    @bt.command(name='remove')
    async def bt_remove(self,ctx):
        pass
    
    async def save_data(self):
        dataIO.save(self._data_path, self.data)

    def get_team(self, server, name_or_id):
        team = None
        teams = self.data(server)
        if name_or_id in teams:
            team = teams[name_or_id]
        else:
            team = discord.utils.get(teams, id=name_or_id)

def get_user(bot, name):
    return discord.utils.get(bot.get_all_members(), name=name)

async def message_owner(bot, msg):
    pass


def check_folders():
    pass

def check_files():
    pass

def setup(bot):
    check_folders()
    check_files()
    bot.add_cog(BTRegistry(bot))